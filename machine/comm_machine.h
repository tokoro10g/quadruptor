#include "config/stm32plus.h"
#include "config/gpio.h"
#include "config/exti.h"
#include "config/event.h"

#include "../devices/nrf24l01p.h"
#include "../utils/bitconverter.h"

#include <Eigen/Core>
#include <Eigen/Geometry>
#include <vector>

using namespace stm32plus;
using namespace Eigen;
using namespace std;

template<class TSpi> 
class CommMachine : public nRF24L01P<TSpi>{
	public:
		CommMachine(typename TSpi::Parameters& params,GpioPinRef cePin,GpioPinRef csnPin):nRF24L01P<TSpi>(params,cePin,csnPin){
			/*
			   os1<<"Address:"<<(int16_t)address[0]<<","<<(int16_t)address[1]<<","<<(int16_t)address[2]<<","<<(int16_t)address[3]<<","<<(int16_t)address[4]<<"\r\n";
			   os1.flush();
			   */
		}
		inline void sendQuaternion(Quaternionf& q){
			uint8_t data[16];
			f2u(q.x(), data);
			f2u(q.y(), data+4);
			f2u(q.z(), data+8);
			f2u(q.w(), data+12);
			nRF24L01P<TSpi>::selectTxMode(address, data, 16);
		}
		inline void selectRxMode(uint8_t len){
			nRF24L01P<TSpi>::selectRxMode(address,len);
		}
		inline void selectTxMode(uint8_t *buf,uint8_t len){
			nRF24L01P<TSpi>::selectTxMode(address,buf,len);
		}
		static const uint8_t address[5];
};

template<class T> 
const uint8_t CommMachine<T>::address[5]={0x34,0x43,0x10,0x10,0x01};
