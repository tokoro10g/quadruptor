#include "config/stm32plus.h"
#include "config/gpio.h"
#include "config/timer.h"

using namespace stm32plus;

template<class TTimer> 
class Eye : public TTimer{
	public:
		Eye():TTimer(){
			TTimer::setTimeBaseByFrequency(200000, 100);
			TTimer::initCompareForPwmOutput();
			TTimer::enablePeripheral();
		}
	private:
};
class Eyes {
	public:
		Eyes():leftEye(),rightEye(){}
		inline void setDutyCycle(uint8_t left,uint8_t right){
			leftEye.setDutyCycle(left);
			rightEye.setDutyCycle(right);
		}
	private:
		Eye<
			Timer1<
				Timer1InternalClockFeature,
				TimerChannel1Feature,
				Timer1GpioFeature<TIMER_REMAP_NONE,TIM1_CH1_OUT>
			>
		> leftEye;
		Eye<
			Timer1<
				Timer1InternalClockFeature,
				TimerChannel4Feature,
				Timer1GpioFeature<TIMER_REMAP_NONE,TIM1_CH4_OUT>
			>
		> rightEye;
};
