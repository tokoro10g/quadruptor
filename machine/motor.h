#include "config/stm32plus.h"
#include "config/gpio.h"
#include "config/timer.h"

#include <Eigen/Core>

using namespace stm32plus;
using namespace Eigen;

class Motors : public Timer3<Timer3InternalClockFeature,TimerChannel1Feature,TimerChannel2Feature,TimerChannel3Feature,TimerChannel4Feature,Timer3GpioFeature<TIMER_REMAP_NONE,TIM3_CH1_OUT,TIM3_CH2_OUT,TIM3_CH3_OUT,TIM3_CH4_OUT> > {
	public:
		enum{
			MAX_COMPARE=100
		};
		Motors():Timer3(){
			this->setTimeBaseByFrequency(2000000, MAX_COMPARE);
			TimerChannel1Feature::initCompareForPwmOutput();
			TimerChannel2Feature::initCompareForPwmOutput();
			TimerChannel3Feature::initCompareForPwmOutput();
			TimerChannel4Feature::initCompareForPwmOutput();
			this->enablePeripheral();
		}
		inline void setDutyAll(uint8_t duty1,uint8_t duty2,uint8_t duty3,uint8_t duty4){
			TimerChannel1Feature::setDutyCycle(duty1);
			TimerChannel2Feature::setDutyCycle(duty2);
			TimerChannel3Feature::setDutyCycle(duty3);
			TimerChannel4Feature::setDutyCycle(duty4);
		}
		inline void setDutyAll(Vector4f duty){
			TimerChannel1Feature::setDutyCycle(limitValue(duty(0)+0.5f,100));
			TimerChannel2Feature::setDutyCycle(limitValue(duty(1)+0.5f,100));
			TimerChannel3Feature::setDutyCycle(limitValue(duty(2)+0.5f,100));
			TimerChannel4Feature::setDutyCycle(limitValue(duty(3)+0.5f,100));
		}
		inline void setCompareAll(uint16_t duty1,uint16_t duty2,uint16_t duty3,uint16_t duty4){
			TimerChannel1Feature::setCompare(duty1);
			TimerChannel2Feature::setCompare(duty2);
			TimerChannel3Feature::setCompare(duty3);
			TimerChannel4Feature::setCompare(duty4);
		}
		inline void setCompareAll(Vector4f duty){
			TimerChannel1Feature::setCompare(limitValue(duty(0)+0.5f,MAX_COMPARE));
			TimerChannel2Feature::setCompare(limitValue(duty(1)+0.5f,MAX_COMPARE));
			TimerChannel3Feature::setCompare(limitValue(duty(2)+0.5f,MAX_COMPARE));
			TimerChannel4Feature::setCompare(limitValue(duty(3)+0.5f,MAX_COMPARE));
		}
		inline void stopAll(){
			setDutyAll(0,0,0,0);
		}
		inline int limitValue(float value,int max){
			if(value>max){
				return max;
			} else if(value<0){
				return 0;
			}
			return (int)value;
		}
	private:
};
