#include "config/stm32plus.h"
#include "config/spi.h"
#include "config/timing.h"
#include "config/gpio.h"
#include "config/event.h"
#include "config/nvic.h"

using namespace stm32plus;

template<class TSpi> 
class nRF24L01P : public TSpi{
public:
	enum{
		REG_CONFIG=0x00,
		REG_EN_AA=0x01,
		REG_EN_RXADDR=0x02,
		REG_SETUP_AW=0x03,
		REG_SETUP_RETR=0x04,
		REG_RF_CH=0x05,
		REG_RF_SETUP=0x06,
		REG_STATUS=0x07,
		REG_OBSERVE_TX=0x08,
		REG_CD=0x09,
		REG_RX_ADDR_P0=0x0a,
		REG_RX_ADDR_P1=0x0b,
		REG_RX_ADDR_P2=0x0c,
		REG_RX_ADDR_P3=0x0d,
		REG_RX_ADDR_P4=0x0e,
		REG_RX_ADDR_P5=0x0f,
		REG_TX_ADDR=0x10,
		REG_RX_PW_P0=0x11,
		REG_RX_PW_P1=0x12,
		REG_RX_PW_P2=0x13,
		REG_RX_PW_P3=0x14,
		REG_RX_PW_P4=0x15,
		REG_RX_PW_P5=0x16,
		REG_FIFO_STATUS=0x17,
		REG_DYNPD=0x1c,
		REG_FEATURE=0x1d,
	};
	enum{
		CMD_R_REGISTER=0x00,
		CMD_W_REGISTER=0x20,
		CMD_R_RX_PAYLOAD=0x61,
		CMD_W_TX_PAYLOAD=0xa0,
		CMD_FLUSH_TX=0xe1,
		CMD_FLUSH_RX=0xe2,
		CMD_REUSE_TX_PL=0xe3,
		CMD_R_RX_PL_WID=0x60,
		CMD_W_ACK_PAYLOAD=0xa8,
		CMD_W_TX_PAYLOAD_NOACK=0xd0,
		CMD_NOP=0xFF
	};
	enum{
		STATUS_RX_DR=0x40,
		STATUS_TX_DS=0x20,
		STATUS_MAX_RT=0x10
	};
	nRF24L01P(typename TSpi::Parameters& params,GpioPinRef _ce,GpioPinRef _csn):TSpi(params),ce(_ce),csn(_csn){
		MillisecondTimer::initialise();
		setCEState(false);
		setCSNState(true);
		MillisecondTimer::delay(5);
		writeReg(REG_EN_AA,0x01);
		writeReg(REG_RF_CH,40);
		writeReg(REG_RF_SETUP,0x0E);
	}
	void setCEState(bool state){
		ce.setState(state);
		/*
		if(state){
			GPIO_WriteBit(GPIOB, GPIO_Pin_8, Bit_SET);
		} else {
			GPIO_WriteBit(GPIOB, GPIO_Pin_8, Bit_RESET);
		}
		*/
	}
	void setCSNState(bool state){
		csn.setState(state);
		/*
		if(state){
			GPIO_WriteBit(GPIOB, GPIO_Pin_9, Bit_SET);
		} else {
			GPIO_WriteBit(GPIOB, GPIO_Pin_9, Bit_RESET);
		}
		*/
	}
	void writeCmd(uint8_t reg,uint8_t val){
		setCSNState(false);
		while(!this->readyToSend());
		this->send(&reg,1);
		while(!this->readyToSend());
		this->send(&val,1);
		setCSNState(true);
	}
	void writeCmd(uint8_t reg,uint8_t *buf,uint8_t len){
		setCSNState(false);
		while(!this->readyToSend());
		this->send(&reg,1);
		while(!this->readyToSend());
		this->send(buf,len);
		setCSNState(true);
	}
	inline void writeReg(uint8_t reg,uint8_t val){
		reg&=0x1f;
		reg|=0x20;
		writeCmd(reg,val);
	}
	inline void writeReg(uint8_t reg,uint8_t *buf,uint8_t len){
		reg&=0x1f;
		reg|=0x20;
		writeCmd(reg,buf,len);
	}
	void readCmd(uint8_t reg,uint8_t *buf,uint8_t len){
		setCSNState(false);
		while(!this->readyToSend());
		this->send(&reg,1);
		this->receive(buf,len);
		setCSNState(true);
	}
	uint8_t readCmd(uint8_t reg){
		setCSNState(false);
		while(!this->readyToSend());
		this->send(&reg,1);
		uint8_t resp;
		this->receive(&resp,1);
		setCSNState(true);
		return resp;
	}
	inline void readReg(uint8_t reg,uint8_t *buf,uint8_t len){
		reg&=0x1f;
		readCmd(reg,buf,len);
	}
	inline uint8_t readReg(uint8_t reg){
		reg&=0x1f;
		return readCmd(reg);
	}
	void selectRxMode(const uint8_t *address,uint8_t len){
		setCEState(false);
		writeReg(REG_RX_ADDR_P0,address,5);
		writeReg(REG_RX_PW_P0,len);
		writeReg(REG_CONFIG,0x0f);
		setCEState(true);
	}
	void selectTxMode(const uint8_t *address,uint8_t *buf,uint8_t len){
		setCEState(false);
		writeReg(REG_TX_ADDR,address,5);
		writeReg(REG_RX_ADDR_P0,address,5);
		writeCmd(CMD_W_TX_PAYLOAD,buf,len);
		writeReg(REG_EN_RXADDR,0x01);
		writeReg(REG_SETUP_RETR,0x1a);
		writeReg(REG_CONFIG,0x0e);
		setCEState(true);
	}
	void clearFlag(){
		writeReg(REG_STATUS,0xff);
	}
	uint8_t getStatus(){
		uint8_t status=readReg(REG_STATUS);
		clearFlag();
		return status;
	}
	void flushTx(){
		writeCmd(CMD_FLUSH_TX, 0xff);
	}
	void flushRx(){
		writeCmd(CMD_FLUSH_RX, 0xff);
	}
	void readPayload(uint8_t *buf,uint8_t len){
		readCmd(CMD_R_RX_PAYLOAD,buf,len);
	}
private:
	GpioPinRef ce;
	GpioPinRef csn;
};
