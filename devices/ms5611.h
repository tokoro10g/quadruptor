
#include "config/stm32plus.h"
#include "config/i2c.h"
#include "config/timing.h"

using namespace stm32plus;

template<class TI2C,int CSB>
class MS5611 : public TI2C{
public:
	enum {
		SLAVE_ADDRESS = (0x76|(!!CSB))<<1
	};
	MS5611(typename TI2C::Parameters& params):TI2C(params){
		MillisecondTimer::initialise();
		this->setSlaveAddress(SLAVE_ADDRESS);
		this->enablePeripheral();
	}
	bool test(){
		unsigned char rx;
		TI2C::readByte(0x75, rx);
		return (rx==0x68);
	}
	/*
	void setup(){
		TI2C::writeByte(0x19, 0x07);
		TI2C::writeByte(0x1b, 0x08);
		//TI2C::writeByte(0x24, {0b01011101});
		//TI2C::writeByte(0x25, {0x80|0x1e});
		TI2C::writeByte(0x6b, 0x00);
	}
	*/
	inline uint32_t readUInt24(uint8_t addr){
		unsigned char rx[3];
		TI2C::readBytes(addr,rx,3);
		uint32_t ret=(rx[0]<<16)|(rx[1]<<8)|rx[2];
		return ret;
	}
private:
};
