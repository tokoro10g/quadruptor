#include "config/stm32plus.h"
#include "config/i2c.h"
#include "config/timing.h"

using namespace stm32plus;

template<class TI2C>
class HMC5883L : public TI2C{
public:
	enum {
		SLAVE_ADDRESS = 0x3C
	};
	HMC5883L(typename TI2C::Parameters& params):TI2C(params){
		MillisecondTimer::initialise();
		this->setSlaveAddress(SLAVE_ADDRESS);
		this->enablePeripheral();
	}
	bool test(){
		unsigned char rx;
		TI2C::readByte(0x0A, rx);
		return (rx==0x48);
	}
	void setup(){
		TI2C::writeByte(0x00, 0x38);
		TI2C::writeByte(0x01, 0x40);
		TI2C::writeByte(0x02, 0x00);
	}
	inline int16_t readInt16(uint8_t addr){
		union{
			uint16_t u;
			int16_t i;
		} _u2i;
		unsigned char rx[2];
		TI2C::readByte(addr,rx[0]);
		TI2C::readByte(addr+1,rx[1]);
		_u2i.u=(rx[0]<<8)|rx[1];
		return _u2i.i;
	}
	inline int16_t readMagX(){
		return readInt16(0x03);
	}
	inline int16_t readMagY(){
		return readInt16(0x05);
	}
	inline int16_t readMagZ(){
		return readInt16(0x07);
	}
	inline void readAll(uint8_t *buf){
		TI2C::readBytes(0x03,buf,6);
	}
private:
};

