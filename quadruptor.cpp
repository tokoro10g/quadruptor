#include "config/stm32plus.h"
#include "config/gpio.h"
#include "config/i2c.h"
#include "config/spi.h"
#include "config/timing.h"
#include "config/usart.h"

#include "devices/mpu6050.h"
#include "devices/ms5611.h"
#include "devices/hmc5883l.h"

#include "machine/comm_machine.h"
#include "machine/eye.h"
#include "machine/motor.h"

#include <Eigen/Dense>
#include "utils/madgwickfilter.h"

using namespace stm32plus;
using namespace Eigen;

inline float spowf(float f){
	return ((f>0)?1.f:-1.f)*f*f;
}

inline float taylorCos(float f){
	return 1-f*f/2.f;
}

inline float taylorSin(float f){
	return f;
}

class Core {
	private:
		enum{
			THRESHOLD_NO_CONTROL=50
		};

		int thrust;
		int roll;
		int pitch;
		long yaw;
		bool estop;

		uint8_t noControlCount;

		Eyes eyes;
		Motors motors;

		/* I2C Devices */
		I2C1_Default<I2CSingleByteMasterPollingFeature>::Parameters i2cParams;
		MPU6050<I2C1_Default<I2CSingleByteMasterPollingFeature>,0> mpu6050;
		MS5611<I2C1_Default<I2CSingleByteMasterPollingFeature>,0> ms5611;
		HMC5883L<I2C1_Default<I2CSingleByteMasterPollingFeature> > hmc5883l;

		/* RF Module */
		GpioB<DefaultDigitalOutputFeature<8,9>,DigitalInputFeature<GPIO_Speed_50MHz,Gpio::PUPD_UP,11> > nrfSysPort;
		GpioPinRef cePin,csnPin;
		Exti11 exti;

		Spi2<>::Parameters spiParams;

		CommMachine<Spi2<> > comm;

		Usart1<> usart1;
		UsartPollingOutputStream _usartos;
		TextOutputStream os1;

	public:
		Core():
			thrust(0),roll(0),pitch(0),yaw(0),estop(false),
			noControlCount(0),
			i2cParams(100000),
			mpu6050(i2cParams),ms5611(i2cParams),hmc5883l(i2cParams),
			nrfSysPort(),
			cePin(nrfSysPort[8]),csnPin(nrfSysPort[9]),
			exti(EXTI_Mode_Interrupt,EXTI_Trigger_Falling,nrfSysPort[11]),
			/* We have to do this since the default constructor exists in class Parameters */
			spiParams([]{
					Spi2<>::Parameters params;
					params.spi_mode=SPI_Mode_Master;
					params.spi_baudRatePrescaler=SPI_BaudRatePrescaler_4;
					params.spi_cpol=SPI_CPOL_Low;
					params.spi_cpha=SPI_CPHA_1Edge;
					return params;
					}()),
			comm(spiParams,cePin,csnPin),
			usart1(921600),_usartos(usart1),os1(_usartos)
			{
				exti.ExtiInterruptEventSender.insertSubscriber(
						ExtiInterruptEventSourceSlot::bind(this, &Core::nRFIRQHandler)
						);
			}
		void run(){
			mpu6050.setTimeout(2);
			ms5611.setTimeout(2);
			hmc5883l.setTimeout(2);

			os1<<"Testing MPU6050...\r\n";
			while(!mpu6050.test());
			os1<<"MPU6050 test passed.\r\n";

			os1<<"Setting up MPU6050...\r\n";
			mpu6050.setup();
			os1<<"complete.\r\n";

			os1<<"Testing HMC5883L...\r\n";
			while(!hmc5883l.test());
			os1<<"HMC5883L test passed.\r\n";

			os1<<"Setting up HMC5883L...\r\n";
			hmc5883l.setup();
			os1<<"complete.\r\n";

			/*
			os1<<"Testing MS5611...\r\n";
			while(!ms5611.test());
			os1<<"MS5611 test passed.\r\n";
			*/

			os1<<"Cleaning up nRF24L01+...\r\n";
			comm.flushTx();
			comm.flushRx();
			os1<<"complete.\r\n";

			for(uint8_t i=0;i<50;i++){
				eyes.setDutyCycle(i*2, i*2);
				MillisecondTimer::delay(10);
			}
			for(uint8_t i=25;i<0;i--){
				eyes.setDutyCycle(i*2+50, i*2+50);
				MillisecondTimer::delay(10);
			}

			MillisecondTimer::delay(1000);

			Vector3f gyrOffset=Vector3f(-12.419f,15.807f,-25.330f);
			MadgwickFilter6DOF filter(0.010f,0.3f);
			os1<<"Setup done.\r\n";

			os1<<"Testing motors...\r\n";
			motors.setDutyAll(15, 15, 15, 15);
			MillisecondTimer::delay(10);
			motors.setDutyAll(0, 0, 0, 0);
			os1<<"complete.\r\n";

			MillisecondTimer::delay(1000);

			uint8_t cnt=0;

			Vector4f omegaI=Vector4f::Zero();
			Vector4f oldOmega=Vector4f::Zero();

			while(1){
				uint8_t mpu[14];
				uint8_t mag[6];
				mpu6050.readAll(mpu);
				hmc5883l.readAll(mag);
				I2C_GenerateSTOP(mpu6050, ENABLE);
				I2C_GenerateSTOP(mpu6050, DISABLE);

				Vector3f Acc(-(float)u2i(mpu),(float)u2i(mpu+2),-(float)u2i(mpu+4));
				Vector3f Gyr((float)u2i(mpu+8),(float)u2i(mpu+10),(float)u2i(mpu+12));
				Gyr-=gyrOffset;
				Gyr*=3.1416f/180.f/16.4f;
				Gyr(0)=-Gyr(0);
				Gyr(2)=-Gyr(2);
				filter.update(Acc, Gyr);

				Quaternionf q=filter.getQuaternion();

				float froll=(float)roll/300.f;
				float fpitch=(float)pitch/200.f;
				float fyaw=(float)yaw/20000.f;
				float sfroll=sin(froll);
				float sfpitch=sin(fpitch);
				float sfyaw=sin(fyaw);
				float cfroll=cos(froll);
				float cfpitch=cos(fpitch);
				float cfyaw=cos(fyaw);

				Vector4f qd_v(
					sfroll*cfpitch*cfyaw-cfroll*sfpitch*sfyaw,
					cfroll*sfpitch*cfyaw+sfroll*cfpitch*sfyaw,
					cfroll*cfpitch*sfyaw-sfroll*sfpitch*cfyaw,
					cfroll*cfpitch*cfyaw+sfroll*sfpitch*sfyaw);
				qd_v.normalize();
				os1<<qd_v(0)<<","<<qd_v(1)<<","<<qd_v(2)<<","<<qd_v(3)<<"\r\n";
				/*
				Vector4f qd_v(0,0,0,1);
				*/

				Vector4f q_v(q.x(),q.y(),q.z(),q.w());

				Matrix4f M;
				M<<q.w(),-q.z(),q.y(),-q.x(),
					q.z(),q.w(),-q.x(),-q.y(),
					-q.y(),q.x(),q.w(),-q.z(),
					q.x(),q.y(),q.z(),q.w();

				Vector4f duty(thrust,thrust,thrust,thrust);
				Vector4f omega=M*(qd_v-q_v);
				if(thrust!=0){
					Vector4f omegaD=omega-oldOmega;
					omegaI+=omega;
					if(omegaI.x()>0.25f) omegaI.x()=0.25f;
					if(omegaI.y()>0.25f) omegaI.y()=0.25f;
					if(omegaI.z()>0.25f) omegaI.z()=0.25f;
					if(omegaI.x()<-0.25f) omegaI.x()=-0.25f;
					if(omegaI.y()<-0.25f) omegaI.y()=-0.25f;
					if(omegaI.z()<-0.25f) omegaI.z()=-0.25f;
					duty(0)+=(+omega.x()+omega.y()+omega.z())*14.f+(+omegaI.x()+omegaI.y()+omegaI.z())*6.0f+(+omegaD.x()+omegaD.y()+omegaD.z())*800.f;
					duty(1)+=(-omega.x()+omega.y()-omega.z())*14.f+(-omegaI.x()+omegaI.y()-omegaI.z())*6.0f+(-omegaD.x()+omegaD.y()-omegaD.z())*800.f;
					duty(2)+=(-omega.x()-omega.y()+omega.z())*14.f+(-omegaI.x()-omegaI.y()+omegaI.z())*6.0f+(-omegaD.x()-omegaD.y()+omegaD.z())*800.f;
					duty(3)+=(+omega.x()-omega.y()-omega.z())*14.f+(+omegaI.x()-omegaI.y()-omegaI.z())*6.0f+(+omegaD.x()-omegaD.y()-omegaD.z())*800.f;
				}
				if(!estop){
					motors.setDutyAll(duty);
				} else {
					eyes.setDutyCycle(0, 0);
				}
				//os1<<duty(0)<<","<<duty(1)<<","<<duty(2)<<","<<duty(3)<<"\r\n";
				//

				if(!nrfSysPort[11].read()){
					uint8_t status=comm.getStatus();
					comm.clearFlag();
					os1<<"isr status:"<<(uint16_t)status<<"\r\n";
					// IRQ from RF module
					if(status&comm.STATUS_RX_DR){
						uint8_t recvLen=comm.readReg(comm.REG_RX_PW_P0);
						os1<<"payload length:"<<(uint16_t)recvLen<<"\r\n";
						if(recvLen==5){
							uint8_t* recv=new uint8_t[recvLen];
							comm.readPayload(recv, recvLen);
							switch(recv[0]){
								case 's':
									eyes.setDutyCycle(0, 0);
									motors.setDutyAll(0,0,0,0);
									estop=true;
									os1<<"\testop:"<<"\r\n";
									break;
								case 'c':
									if(!estop){
										if(thrust-recv[1]<10){
											thrust=(float)thrust*0.6f+(float)((int8_t)recv[1])*0.4f;
										} else {
											thrust=(float)thrust*0.9f+(float)((int8_t)recv[1])*0.1f;
										}
										roll=(float)roll*0.7f-(float)((int8_t)recv[2])*0.3f;
										pitch=(float)pitch*0.7f+(float)((int8_t)recv[3])*0.3f;
										yaw+=(int8_t)recv[4];
										os1<<"\tcontrol:"<<(int16_t)thrust<<","<<(int16_t)roll<<","<<(int16_t)pitch<<","<<(int16_t)yaw<<"\r\n";
									}
									break;
								case 'e':
									eyes.setDutyCycle(recv[1], recv[2]);
									break;
								default:
									break;
							}
							delete recv;
							noControlCount=0;
						}
					} else {
						noControlCount++;
					}
					if(status&comm.STATUS_TX_DS){
						comm.selectRxMode(5);
					}
					if(status&comm.STATUS_MAX_RT){
						comm.flushTx();
						comm.selectRxMode(5);
					}
				} else {
					noControlCount++;
				}

				if(noControlCount>=THRESHOLD_NO_CONTROL){
					thrust=(int)((float)thrust*0.95f);
					if(thrust==0){
						motors.setDutyAll(Vector4f::Zero());
						roll=0;
						pitch=0;
						yaw=0;
						estop=true;
					}
				}

				if(cnt%4==0){
					os1<<"send quaternion\r\n";
					comm.sendQuaternion(q);
				}
				MillisecondTimer::delay(7);
				oldOmega=omega;
				cnt++;
			}
		}
		void nRFIRQHandler(uint8_t extiLine){
			if(extiLine==11){
			}
			exti.clearPendingInterrupt();
		}
};

int main() {
	SystemInit();
	SystemCoreClockUpdate();
	Core core;
	core.run();
}
