#include <Eigen/Core> 
#include <Eigen/Geometry> 
#include <Eigen/Dense>

using namespace Eigen;

class Filter {
	public:
		Filter(float _dt):q(AngleAxisf(0.0,Vector3f::Zero())),dt(_dt){}
		Quaternionf getQuaternion() const { return q; }
		Matrix3f getRotationMatrix() const { return q.toRotationMatrix(); }
	protected:
		Quaternionf q;
		float dt;
};
