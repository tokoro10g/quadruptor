#include <Eigen/Core> 
#include <Eigen/Geometry> 
#include <Eigen/Dense>
#include "filter.h"

using namespace Eigen;

class MadgwickFilter6DOF : public Filter {
	public:
		MadgwickFilter6DOF(float _dt,float _beta):Filter(_dt),beta(_beta){}
		void update(Vector3f Acc,Vector3f Gyr){
			Vector3f F=q.conjugate().toRotationMatrix()*Vector3f(0,0,1);
			F-=Acc.normalized();
			MatrixXf J(3,4);
			J<<2.f*q.z(),-2.f*q.w(),2.f*q.x(),-2.f*q.y(),
				2.f*q.w(),2.f*q.z(),2.f*q.y(),2.f*q.x(),
				-4.f*q.x(),-4.f*q.y(),0,0;
			Vector4f step=J.transpose()*F;
			step.normalize();

			Quaternionf sens_q(Vector4f(Gyr(0),Gyr(1),Gyr(2),0));
			Quaternionf qDot_q=q*sens_q;

			Vector4f qDot=0.5f*Vector4f(qDot_q.x(),qDot_q.y(),qDot_q.z(),qDot_q.w())-beta*step;
			qDot*=dt;

			q=Quaternionf(Vector4f(q.x(),q.y(),q.z(),q.w())+qDot);
			q.normalize();
		}
	private:
		float beta;
};

class MadgwickFilter9DOF : public Filter {
	public:
		MadgwickFilter9DOF(float _dt,float _beta):Filter(_dt),beta(_beta){}
		void update(Vector3f Acc,Vector3f Gyr,Vector3f Mag){

		}
	private:
		float beta;
};
